package id.telkomsigma.training.marketplace.storex.dto;

//import lombok.Data;
import id.telkomsigma.training.marketplace.storex.dto.CartItem;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

//@Data 
public class ShoppingCart {
    private List<CartItem> isiCart = new ArrayList<>();

    public BigDecimal totalBerat(){
        BigDecimal total = BigDecimal.ZERO;
        for (CartItem ci : isiCart) {
            total = total.add(ci.getProduct().getWeight().multiply(BigDecimal.valueOf(ci.getJumlah())));
        }
        return total;
    }

	public List<CartItem> getIsiCart() {
		return isiCart;
	}

	public void setIsiCart(List<CartItem> isiCart) {
		this.isiCart = isiCart;
	}
    
    
}
