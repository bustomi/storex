package id.telkomsigma.training.marketplace.storex.dto;

//import lombok.Data;
//
//@Data
public class CartItem {
    private Product product;
    private Integer jumlah;
    
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public Integer getJumlah() {
		return jumlah;
	}
	public void setJumlah(Integer jumlah) {
		this.jumlah = jumlah;
	}
    
    
}
