package id.telkomsigma.training.marketplace.storex.backendservices;

import id.telkomsigma.training.marketplace.storex.dto.Product;
import id.telkomsigma.training.marketplace.storex.dto.ProductPhotos;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;

@FeignClient(value = "catalogx", fallback = CatalogService.CatalogFallback.class)
public interface CatalogService {
    // http://catalog/api/product/p001
    @RequestMapping(method = RequestMethod.GET, value = "/api/product/{id}", consumes = "application/json")
    public Product cariProdukById(@PathVariable("id") String id);

    @RequestMapping(method = RequestMethod.GET, value = "/api/product/{id}/photos", consumes = "application/json")
    public Iterable<ProductPhotos> fotoProdukById(@PathVariable("id") String id);

    static class CatalogFallback implements CatalogService{
        @Override
        public Product cariProdukById(String id) {
            return null;
        }

        @Override
        public Iterable<ProductPhotos> fotoProdukById(String id) {
            return new ArrayList<>();
        }
    }
}
