package id.telkomsigma.training.marketplace.storex.dto;

//import lombok.Data;

//@Data
public class ProductPhotos {
    private String id;
    private Product product;
    private String url;
    
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
    
}
