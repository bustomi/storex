package id.telkomsigma.training.marketplace.storex.dto;

//import lombok.Data;

import java.math.BigDecimal;

//@Data
public class Shipment {
    private String provider;
    private String jenis;
    private String asal;
    private String tujuan;
    private BigDecimal berat;
    private BigDecimal biaya;
    
	public String getProvider() {
		return provider;
	}
	public void setProvider(String provider) {
		this.provider = provider;
	}
	public String getJenis() {
		return jenis;
	}
	public void setJenis(String jenis) {
		this.jenis = jenis;
	}
	public String getAsal() {
		return asal;
	}
	public void setAsal(String asal) {
		this.asal = asal;
	}
	public String getTujuan() {
		return tujuan;
	}
	public void setTujuan(String tujuan) {
		this.tujuan = tujuan;
	}
	public BigDecimal getBerat() {
		return berat;
	}
	public void setBerat(BigDecimal berat) {
		this.berat = berat;
	}
	public BigDecimal getBiaya() {
		return biaya;
	}
	public void setBiaya(BigDecimal biaya) {
		this.biaya = biaya;
	}
    
    
}
