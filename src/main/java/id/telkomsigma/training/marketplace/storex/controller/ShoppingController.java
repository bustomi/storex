package id.telkomsigma.training.marketplace.storex.controller;

import id.telkomsigma.training.marketplace.storex.backendservices.CatalogService;
import id.telkomsigma.training.marketplace.storex.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

@RestController
@SessionAttributes("cart")
public class ShoppingController {

    @Autowired private CatalogService catalogService;

    @ModelAttribute("cart")
    public ShoppingCart createCart(){
        return new ShoppingCart();
    }

    @GetMapping("/api/product/{id}")
    public Map<String, Object> productDetail(@PathVariable("id") String product){
        Map<String, Object> data = new LinkedHashMap<>();

        Product p = catalogService.cariProdukById(product);
        data.put("product", p);
        data.put("photos", catalogService.fotoProdukById(product));

        return data;
    }

    @PostMapping("/api/add")
    public ShoppingCart addToCart(@RequestParam("product") String product,
                                 @RequestParam("jumlah") Integer jumlah,
                                  @ModelAttribute("cart") ShoppingCart cart){
        Product p001 = new Product();
        p001.setId(product);
        p001.setCode("P-001");
        p001.setName("Product 001");
        p001.setWeight(new BigDecimal(23.4));
        p001.setPrice(new BigDecimal(12345.67));

        CartItem ci = new CartItem();
        ci.setProduct(p001);
        ci.setJumlah(jumlah);

        cart.getIsiCart().add(ci);

        return cart;
    }

    @GetMapping("/api/cart")
    public ShoppingCart viewCart(@ModelAttribute("cart") ShoppingCart cart){
        return cart;
    }

    @PostMapping("/api/order")
    public PurchaseOrder order(@RequestParam String shipping, @RequestParam String jenis,
                               @ModelAttribute("cart") ShoppingCart cart){
        PurchaseOrder po = new PurchaseOrder();
        po.setDaftarBelanja(cart.getIsiCart());

        Shipment ship = new Shipment();
        ship.setProvider(shipping);
        ship.setJenis(jenis);
        ship.setBerat(cart.totalBerat());

        po.setPengiriman(ship);
        return po;
    }
}
