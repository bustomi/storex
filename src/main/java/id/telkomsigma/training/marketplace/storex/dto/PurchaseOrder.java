package id.telkomsigma.training.marketplace.storex.dto;

//import lombok.Data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

//@Data
public class PurchaseOrder {
    private Date waktuTransaksi = new Date();
    private List<CartItem> daftarBelanja = new ArrayList<>();
    private Shipment pengiriman = new Shipment();
    
	public Date getWaktuTransaksi() {
		return waktuTransaksi;
	}
	public void setWaktuTransaksi(Date waktuTransaksi) {
		this.waktuTransaksi = waktuTransaksi;
	}
	public List<CartItem> getDaftarBelanja() {
		return daftarBelanja;
	}
	public void setDaftarBelanja(List<CartItem> daftarBelanja) {
		this.daftarBelanja = daftarBelanja;
	}
	public Shipment getPengiriman() {
		return pengiriman;
	}
	public void setPengiriman(Shipment pengiriman) {
		this.pengiriman = pengiriman;
	}
    
    
}
